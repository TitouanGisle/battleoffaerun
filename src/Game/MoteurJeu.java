
package Game;

import Game.Classes.Guerrier;
import Game.Classes.Plateau;
import Game.view.Menestrel;

import java.util.ArrayList;
import java.util.Scanner;

public class MoteurJeu {
    public static void main(String[] args) {
        Plateau plateau = new Plateau();
        int tour = 1;
        while (!plateau.finDuJeu()) {
            Menestrel.nouveauTour(tour);
            plateau.avancerToutLeMonde();
            //plateau.avancerFaction1();
            //plateau.avancerFaction2();

            //Entrainement des troupes des chateaux
            plateau.getChateauFaction1().entrainementRandom(3);
            plateau.getChateauFaction2().entrainementRandom(3);

            ArrayList<Guerrier> guerriersChateau1 = plateau.getChateauFaction1().diplomer();
            plateau.getCarreaux().get(0).setGuerriersFaction1(guerriersChateau1);
            Menestrel.deploiementTroupes(plateau.getChateauFaction1(), guerriersChateau1);

            ArrayList<Guerrier> guerriersChateau2 = plateau.getChateauFaction2().diplomer();
            plateau.getCarreaux().get(plateau.getCarreaux().size()-1).setGuerriersFaction2(guerriersChateau2);
            Menestrel.deploiementTroupes(plateau.getChateauFaction2(), guerriersChateau2);

            Menestrel.decrirePlateau(plateau);

            plateau.chercherLaBagarre();

            plateau.getChateauFaction1().leverImpot();
            plateau.getChateauFaction2().leverImpot();

            System.out.println("Appuyez sur 'Entrée' pour passer au tour suivant.");
            new Scanner(System.in).nextLine();

            tour++;
        }
        System.out.println("------------- Fin du Jeu -------------------");
        System.out.println("L'état du plateau en ce tour de Grâce "+tour);
        Menestrel.decrirePlateau(plateau);

        System.out.println("C'est donc le joueur "+plateau.gagnant()+" qui gagne !");
    }

}
