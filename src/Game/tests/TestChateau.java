package Game.tests;

import Game.Classes.Chateau;
import Game.Classes.Guerrier;

import java.util.ArrayList;
import java.util.Random;

public class TestChateau {
    public static void main(String[] args) {
        Chateau chateau = new Chateau("vert pomme");

        for(int i = 0 ; i<3; i++) {
            Guerrier apprenti = chateau.entrainer(1, false);
            System.out.println(apprenti.getGroupeNominal() + " a commencé son entraînement.");
        }

        //ajout de 10 unités à la salle d'entrainement
        for (int i = 0; i<5; i++) {
            boolean chef = new Random().nextBoolean();
            int type = new Random().nextInt(2);

            Guerrier apprenti = chateau.entrainer(type, chef);
            System.out.println(apprenti.getGroupeNominal() + " a commencé son entraînement.");
        }

        ArrayList<Guerrier> bleusaille;
        int tour = 1;
        while (!chateau.getSalleDEntrainement().isEmpty()) {
            System.out.println("\n------------------- Tour "+tour+" -----------------------------");
            System.out.println(chateau.getRessources()+" ressources sont disponibles");

            bleusaille = chateau.diplomer();
            if(bleusaille.isEmpty()) {
                System.out.println("Aucune unité n'a pu être entraînée à ce tour, mais "+chateau.getSalleDEntrainement().peek().getGroupeNominal()+" est bientôt prêt !");
            } else {
                for ( Guerrier guerrier: bleusaille ) {
                    System.out.println(guerrier.getGroupeNominal() + " est prêt au combat ! (Il a coûté "+guerrier.getCout()+" ressources.)");
                }
            }

            chateau.leverImpot();
            tour++;
        }
    }
}
