package Game.tests;

import Game.Classes.Carreau;
import Game.Classes.Chateau;
import Game.Classes.Guerrier;

import java.util.Collections;
import java.util.Scanner;

public class TestTriCombat {
    public static void main(String[] args) {
        Chateau chateau1 = new Chateau("vert");
        Chateau chateau2 = new Chateau("mauve");
        Carreau carreau = new Carreau();

        //un gros tas de ressources
        chateau1.setRessources(24);
        chateau2.setRessources(24);

        //un gros tas d'unités prêtes à être déposées
        chateau1.entrainementRandom(24);
        chateau2.entrainementRandom(24);

        //déploiement des troupes
        carreau.setGuerriersFaction1(chateau1.diplomer());
        carreau.setGuerriersFaction2(chateau2.diplomer());

        Collections.sort(carreau.getGuerriersFaction1(), Guerrier.guerrierComparator);
        Collections.sort(carreau.getGuerriersFaction2(), Guerrier.guerrierComparator);

        System.out.println("Troupes triées : ");
        affichage(carreau);

        int i=0;
        do {
            System.out.print("Appuyez sur Entrée pour lancer un tour de combat");
            new Scanner(System.in).nextLine();
            System.out.println("----------Tour "+ ++i +"----------");
            carreau.combattre();

            Collections.sort(carreau.getGuerriersFaction1(), Guerrier.guerrierComparator);
            Collections.sort(carreau.getGuerriersFaction2(), Guerrier.guerrierComparator);

            affichage(carreau);
        } while(!(carreau.getGuerriersFaction1().isEmpty() || carreau.getGuerriersFaction2().isEmpty()));


    }

    private static void affichage(Carreau carreau) {
        int i = 0;

        System.out.println("|  Chateau 1  ||  Chateau  2 |");
        System.out.println("|-------------||-------------|");
        while(!(i>=carreau.getGuerriersFaction2().size() && i>=carreau.getGuerriersFaction1().size())) {

            if(i<carreau.getGuerriersFaction1().size()) {
                System.out.print("| "+carreau.getGuerriersFaction1().get(i).getCode()+"("+carreau.getGuerriersFaction1().get(i).getCurrentPV()+"/100) |");
            } else {
                System.out.print("|             |");
            }

            if(i<carreau.getGuerriersFaction2().size()) {
                System.out.print("| "+carreau.getGuerriersFaction2().get(i).getCode()+"("+carreau.getGuerriersFaction2().get(i).getCurrentPV()+"/100) |");
            } else {
                System.out.print("|             |");
            }

            System.out.print("\n");
            i++;
        }
    }
}
