package Game.tests;

import Game.exceptions.CoupDivin;
import Game.Classes.GuerrierElf;
import Game.Classes.GuerrierNain;

/**
 * Class TestGuerrier
 */
public class TestGuerrier {
  public static void main(String[] args) {
    GuerrierElf guerrierElf = new GuerrierElf();
    GuerrierNain guerrierNain = new GuerrierNain();

    while (guerrierElf.getCurrentPV()>0 && guerrierNain.getCurrentPV()>0) {
      try {
        guerrierNain.attaquer(guerrierElf);
        guerrierElf.attaquer(guerrierNain);
      } catch (CoupDivin cp) {

      }


      System.out.println("PV d'"+ guerrierNain.getGroupeNominal()+" : "+ guerrierNain.getCurrentPV()+"/"+ guerrierNain.getBasePv());
      System.out.println("PV d'"+ guerrierElf.getGroupeNominal()+" : "+ guerrierElf.getCurrentPV()+"/"+ guerrierElf.getBasePv());
    }
  }


}
