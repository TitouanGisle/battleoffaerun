package Game.tests;

import Game.Classes.Carreau;
import Game.Classes.Guerrier;
import Game.Classes.Plateau;

import java.util.ArrayList;

public class TestPlateau2 {
    public static void main(String[] args) {
        Plateau plateau = new Plateau();



        int tour = 1;
        while(tour<=20) {
            plateau.getChateauFaction2().leverImpot();
            plateau.getChateauFaction2().entrainementRandom(4);

            ArrayList<Guerrier> guerriersChateau1 = plateau.getChateauFaction2().diplomer();
            plateau.getCarreaux().get(4).setGuerriersFaction2(guerriersChateau1);

            System.out.println("----Tour "+tour+" : le guerrier avance.");
            plateau.avancerFaction2();
            decrirePlateau(plateau);
            tour++;
        }

        System.out.println("Troupes arrivées au chateau adverse :");
        for (Guerrier guerrier: plateau.getCarreaux().get(4).getGuerriersFaction2()) {
            System.out.println(guerrier.getGroupeNominal());
        }

    }

    public static void decrirePlateau(Plateau plateau) {
        int ic = 0;
        for (Carreau carreau: plateau.getCarreaux()) {
            System.out.println("----- "+ic+" -----");
            if(carreau.getGuerriersFaction1().isEmpty() && carreau.getGuerriersFaction2().isEmpty()) {
                System.out.println("|    Vide    |");
            } else {
                System.out.print("|");
                for (Guerrier guerrier : carreau.getGuerriersFaction1()) {
                    System.out.print(guerrier.getCout()+"R, ");
                }
                for (Guerrier guerrier : carreau.getGuerriersFaction2()) {
                    System.out.print(guerrier.getCout()+"B, ");
                }
                System.out.print("|\n");
            }

            ic++;
        }
    }
}
