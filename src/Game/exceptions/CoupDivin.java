package Game.exceptions;

import Game.Classes.Guerrier;

public class CoupDivin extends Exception{
    private Guerrier guerrier;

    public CoupDivin(Guerrier guerrier) {
        super();
        this.guerrier = guerrier;
    }

    public Guerrier getGuerrier() {
        return guerrier;
    }
}
