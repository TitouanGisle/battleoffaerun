package Game.view;

import Game.Classes.Carreau;
import Game.Classes.Chateau;
import Game.Classes.Guerrier;
import Game.Classes.Plateau;
import Game.exceptions.CoupDivin;

import java.util.ArrayList;

/**
 * Classe chargée de l'affichage
 */
public class Menestrel {

    public static void nouveauTour(int tour) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("--------------------------- Tour "+tour+"-----------------------------");
        System.out.println("----------------------------------------------------------------");
        System.out.println("\n - Les troupes en jeu se ruent vers le château adverse !");
    }

    public static void deploiementTroupes(Chateau chateau, ArrayList<Guerrier> guerriers) {
        System.out.println(" - Le chateau "+chateau.getCouleur()+" a "+chateau.getRessources()+" ressources disponibles.");
        if (guerriers.isEmpty()) {
            System.out.println("   Il a en effet préféré attendre.");
        } else {
            System.out.println("   Il a en effet déployé :");
                for (Guerrier guerrier : guerriers) {
                    System.out.println("    - "+guerrier.getGroupeNominal());
                }
        }
    }

    public static void conterAttaque(Guerrier attaquant, Guerrier défenseur, int degats) {
        System.out.print(attaquant.getGroupeNominal()+" a attaqué "+défenseur.getGroupeNominal()+" ! Il lui a fait "+degats+" dégats !\n");
    }

    public static void annoncerDeces(Guerrier guerrier) {
        System.out.println(guerrier.getGroupeNominal()+" a péri au combat ! Puisse son âme reposer auprès des dieux !");
    }

    public static void narrerCoupDivin(CoupDivin cp) {
        System.out.println(cp.getGuerrier().getGroupeNominal()+"a bénéficié de l'aide des dieux, et a exterminé tous ses adversaires!");
    }

    public static void rapporterHecatombe(String couleur) {
        System.out.println("C'est une hécatombe ! Tous les guerriers "+couleur+"s de cette case ont péri !");
    }

    public static void debuterCombat(int carreau) {
        System.out.println("Un combat éclate sur la case "+carreau+" !");
    }

    public static void decrirePlateau(Plateau plateau) {
        int ic = 0;
        System.out.println("   Voyez donc l'état du champs de bataille :");

        System.out.println("[Chateau "+plateau.getChateauFaction1().getCouleur()+"]");
        for (Carreau carreau: plateau.getCarreaux()) {
            System.out.println("-------- "+ic+" --------");
            if(carreau.getGuerriersFaction1().isEmpty() && carreau.getGuerriersFaction2().isEmpty()) {
                System.out.println("|       Vide       |");
            } else {
                System.out.print("|");
                for (Guerrier guerrier : carreau.getGuerriersFaction1()) {
                    System.out.print(guerrier.getCode()+"("+guerrier.getCurrentPV()+"/"+guerrier.getBasePv()+"), ");
                }
                for (Guerrier guerrier : carreau.getGuerriersFaction2()) {
                    System.out.print(guerrier.getCode()+"("+guerrier.getCurrentPV()+"/"+guerrier.getBasePv()+"), ");
                }
                System.out.print("|\n");
            }

            ic++;
        }
        System.out.println("[Chateau "+plateau.getChateauFaction2().getCouleur()+"]");
    }
}
