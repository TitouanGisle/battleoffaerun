package Game.Classes;

import Game.exceptions.CoupDivin;
import Game.view.Menestrel;

import java.util.*;


/**
 * Class Guerrier
 */
abstract public class Guerrier {

    //
    // Fields
    //

    static protected final int BASE_FORCE = 10;
    static protected final int BASE_PV = 100;
    static protected final int BASE_COUT = 1;
    protected int currentPV;
    protected boolean chef = false;
    protected String Couleur;
    protected Chateau chateau;
    //plutôt que couleur, lien vers son château, qui sait sa faction, et sa couleur

    //
    // Constructors
    //
    public Guerrier () {
        setCurrentPV(getBasePv());
    };

    public Guerrier (boolean chef) {
        setCurrentPV(getBasePv());
        setChef(chef);
    };

    public Chateau getChateau() {
        return chateau;
    }

    public void setChateau(Chateau chateau) {
        this.chateau = chateau;
    }
    public String getCouleur() {
        return (this.chateau == null) ? "sans couleur" : chateau.getCouleur();
    }

    public static Comparator<Guerrier> guerrierComparator  = new Comparator<Guerrier>() {
        public int compare(Guerrier guerrier1, Guerrier guerrier2) {
            //On s'appuie sur la valeur de témérité du guerrier pour les comparer
            //Le guerrier le plus téméraire joue en premier
            //Tel que N(x/100)>n(100/100)>n(50/100)>e(x/100)>E(x/100)
            return guerrier2.getTemerite()-guerrier1.getTemerite();
        }
    };

    public abstract int getTemerite();


    //
    // Accessor methods
    //

    /**
     * Get the value of baseForce
     * @return the value of baseForce
     */
    public int getForce () {
        return BASE_FORCE;
    }

    /**
     * Get the value of basePv
     * @return the value of basePv
     */
    public int getBasePv () {
        return BASE_PV;
    }

    /**
     * Set the value of currentPV
     * @param newVar the new value of currentPV
     */
    public void setCurrentPV (int newVar) {
        currentPV = newVar;
    }

    /**
     * Get the value of currentPV
     * @return the value of currentPV
     */
    public int getCurrentPV () {
        return currentPV;
    }
    /**
     * @return  int
     */

    abstract public int getCout();

    /**
     * Set the value of chef
     * @param newVar the new value of chef
     */
    public void setChef (boolean newVar) {
        chef = newVar;
    }


    /**
     * Get the value of chef
     * @return       boolean
     */
    public boolean isChef() { return chef; }


    /**
     * Throw qte three-sided dice
     * @return       int
     */
    public int de3(int qte)
    {
        int total = 0;
        for (int i = 0 ; i<qte ; i++ ) {
            total += new Random().nextInt(3)+1;
        }
        return total;
    }


    /**
     * @return       int
     * @param        degatsCauses
     */
    public int defendre(int degatsCauses)
    {
        setCurrentPV(getCurrentPV()-degatsCauses);
        return degatsCauses;
    }

    /**
     * @return       int
     */
    public int attaquer(Guerrier cible) throws CoupDivin {
        int degatsCauses = de3(getForce());
        //Si les dégats sont supérieurs à 50% du maximum de dégat possible (valeur de test)
        if(degatsCauses>0.9*getForce()*3) {
            throw new CoupDivin(this);
        }
        int degatsReels = cible.defendre(degatsCauses);
        Menestrel.conterAttaque(this, cible, degatsReels);

        return degatsReels;
    }

    /**
     * retourne un string qui décrit le type d'unité
     */
    public abstract String getGroupeNominal();

    /**
     * indique si un guerrier est mort
     * @return      boolean
     */
    public boolean isDead() {
        return currentPV<=0;
    }

    public abstract String getCode();
}
