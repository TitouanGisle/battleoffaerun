package Game.Classes;

import Game.view.Menestrel;

import java.util.ArrayList;
import java.util.Collections;

public class Plateau {
    private static final int NOMBRE_CARREAUX = 5;
    private Chateau chateauFaction1;
    private Chateau chateauFaction2;
    private ArrayList<Carreau> carreaux;
    private ArrayList<Guerrier> guerriersEnMarche;

    public Plateau() {
        carreaux = new ArrayList<Carreau>();
        for(int i = 0 ; i<NOMBRE_CARREAUX; i++ ) {
            carreaux.add(new Carreau());
        }
        chateauFaction1 = new Chateau("rouge");
        chateauFaction2 = new Chateau("bleu");
        guerriersEnMarche = new ArrayList<Guerrier>();
    }

    public Chateau getChateauFaction1() {
        return chateauFaction1;
    }

    public Chateau getChateauFaction2() {
        return chateauFaction2;
    }

    public ArrayList<Carreau> getCarreaux() {
        return carreaux;
    }

    /**
     * Parcourt les carreaux du tableau, et fait avancer les troupes qui s'y trouve,
     * à condition qu'elles soient seules sur le carreau.
     */
    public void avancerFaction2() {
        //on parcourt le plateau de gauche à droite (0->14)
        for(int i = 1 ; i<NOMBRE_CARREAUX ; i++) {
            // il y a des guerriers de la faction 2, et pas de guerriers de la faction 1
            if(carreaux.get(i).getGuerriersFaction1().isEmpty() && !carreaux.get(i).getGuerriersFaction2().isEmpty()){
                //les guerriers de la faction2 avancent : on met les guerriers du carreau i dans le carreau i-1
                ArrayList<Guerrier> guerriers = new ArrayList<>(carreaux.get(i).getGuerriersFaction2());
                carreaux.get(i-1).getGuerriersFaction2().addAll(guerriers);
                //puis on vide le carreau i
                carreaux.get(i).getGuerriersFaction2().clear();
            }
        }
    }

    public void avancerFaction1() {
        //on parcourt le plateau de droite à gauche (0<-14)
        for (int i = NOMBRE_CARREAUX - 2; i >= 0; i--) {
            //s'il y a des guerriers de la faction1, et pas de guerriers de la faction 2
            if (!carreaux.get(i).getGuerriersFaction1().isEmpty() && carreaux.get(i).getGuerriersFaction2().isEmpty()) {
                //les guerriers de la faction1 avancent : on met les guerriers du carreau i dans le carreau i+1
                ArrayList<Guerrier> guerriers = new ArrayList<>(carreaux.get(i).getGuerriersFaction1());
                carreaux.get(i+1).getGuerriersFaction1().addAll(guerriers);
                //puis on vide le carreau i
                carreaux.get(i).getGuerriersFaction1().clear();
            }
        }
    }

    /**
     * Parcourt les carreau du plateau, lance un combat si deux troupes opposées s'y trouves
     */
    public void chercherLaBagarre() {
        for (int i = 0; i < NOMBRE_CARREAUX; i++) {
            if(!carreaux.get(i).getGuerriersFaction1().isEmpty() && !carreaux.get(i).getGuerriersFaction2().isEmpty()) {
                Menestrel.debuterCombat(i);
                carreaux.get(i).combattre();
            }
        }
    }

    /**
     * Retourne true si des guerriers de la faction2 sont sur la case 0,
     * ou si des guerriers de la faction1 sont sur la case 14
     * @return
     */
    public boolean finDuJeu() {
        return carreaux.get(0).getGuerriersFaction2().size()>0 || carreaux.get(NOMBRE_CARREAUX-1).getGuerriersFaction1().size()>0;
    }

    public String gagnant() {
        if (finDuJeu()) {
            //S'il y a des guerriers de la faction 2 sur le carreau 0, c'est faction 1 qui gagne
            //sinon c'est faction 1
            return !carreaux.get(0).getGuerriersFaction2().isEmpty() ? chateauFaction2.getCouleur() : chateauFaction1.getCouleur();
        } else {
            return "aucun";
        }
    }

    public void avancerToutLeMonde() {
        int i = 0;
        ArrayList<Guerrier> tamponGuerriers = new ArrayList<Guerrier>();

        while(i < NOMBRE_CARREAUX) {
            //s'il n'y a pas de guerriers de la faction 2,
            if(carreaux.get(i).getGuerriersFaction2().isEmpty()) {
                //on met les guerriers de la faction 1 dans le tampon,
                tamponGuerriers = new ArrayList<>(carreaux.get(i).getGuerriersFaction1());
                carreaux.get(i).getGuerriersFaction1().clear();
            }
            //on ajoute à cette case les guerriers en déplacement
            carreaux.get(i).getGuerriersFaction1().addAll(this.guerriersEnMarche);
            this.guerriersEnMarche = new ArrayList<>(tamponGuerriers);
            tamponGuerriers.clear();

            //S'il n'y a pas de guerriers de la faction 1
            if(carreaux.get(i).getGuerriersFaction1().isEmpty() && i>0) {
                //on avance les guerriers de la faction 2 "simplement", en les passant au carreau i-1
                ArrayList<Guerrier> guerriersFac2 = new ArrayList<>(carreaux.get(i).getGuerriersFaction2());
                carreaux.get(i-1).getGuerriersFaction2().addAll(guerriersFac2);
                //puis on vide le carreau i des guerriers de la faction
                carreaux.get(i).getGuerriersFaction2().clear();
            }
            //On trie les guerriers sur la case
            Collections.sort(carreaux.get(i).getGuerriersFaction1(), Guerrier.guerrierComparator);
            Collections.sort(carreaux.get(i).getGuerriersFaction2(), Guerrier.guerrierComparator);

            i++;
        }
    }
}
