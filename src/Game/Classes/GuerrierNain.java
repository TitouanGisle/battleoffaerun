package Game.Classes;

/**
 * Class GuerNain
 */
public class GuerrierNain extends Guerrier {

  //
  // Fields
  //

  static private int multDef = 2;
  
  //
  // Constructors
  //
  public GuerrierNain() {
      super();
  };

  public int getCout() {
      return isChef() ? BASE_COUT*3 : BASE_COUT;
  }

  /**
   * Get the value of multDef
   * @return the value of multDef
   */
  protected int getMultDef ()
  {
    return isChef() ? multDef*2 : multDef;
  }

  //
  // Other methods
  //

  /**
   * @return       int
   * @param        degatsCauses
   */
  public int defendre(int degatsCauses)
  {
      int degatsReels = degatsCauses/getMultDef();
      setCurrentPV(getCurrentPV()-degatsReels);

      return degatsReels;
  }

  public String getGroupeNominal() {
      String gn = isChef() ? "un chef" : "un guerrier";
      gn = gn+" nain "+getCouleur();
      return gn;
  }
    /**
     * Retourne un code pour reconnaître l'unité
     * N : chef nain    n : guerrier nain
     * E : chef elfe    e : guerrier elfe
     */
    public String getCode() {
        return (isChef()) ? "N"+this.getCouleur().toUpperCase().charAt(0) : "n"+this.getCouleur().toUpperCase().charAt(0);
    }

    //Attribue une valeur de temerite à l'unité (plus c'est petit, plus elle se reculera dans l'ordre de defense)
    //Tel que N(x/100)>n(100/100)>n(50/100)>e(x/100)>E(x/100)
    public int getTemerite() {
        return isChef() ? 4000+getCurrentPV() : 3000+getCurrentPV();
    }
}
