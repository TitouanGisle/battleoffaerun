package Game.Classes;

import Game.exceptions.CoupDivin;
import Game.view.Menestrel;

import java.util.ArrayList;

public class Carreau {
    private ArrayList<Guerrier> guerriersFaction1;
    private ArrayList<Guerrier> guerriersFaction2;

    public Carreau() {
        guerriersFaction1 = new ArrayList<Guerrier>();
        guerriersFaction2 = new ArrayList<Guerrier>();
    }


    public ArrayList<Guerrier> getGuerriersFaction1() {
        return guerriersFaction1;
    }

    public ArrayList<Guerrier> getGuerriersFaction2() {
        return guerriersFaction2;
    }

    public void setGuerriersFaction1(ArrayList<Guerrier> guerriersFaction1) {
        this.guerriersFaction1 = guerriersFaction1;
    }

    public void setGuerriersFaction2(ArrayList<Guerrier> guerriersFaction2) {
        this.guerriersFaction2 = guerriersFaction2;
    }

    /**
     * Fait combattre les guerriers de la faction1 aux guerriers de la faction2 présents sur la case
     *
     */
    public void combattre() {
        int i = 0;
        //tant que i n'est pas supérieur à la taille des deux armées
        //Et qu'aucune armées n'est vide
        while(!(i>=guerriersFaction1.size() && i>=guerriersFaction2.size()) && !(guerriersFaction1.size()==0) && !(guerriersFaction2.size()==0)) {
            //les deux geurriers adverse, ou le guerrier dont c'est le tour attaque(nt) le premier guerrier de l'armée adverse
            try {
                if(guerriersFaction1.size()>i){
                    guerriersFaction1.get(i).attaquer(guerriersFaction2.get(0));
                }

                if(guerriersFaction2.size()>i) {
                    guerriersFaction2.get(i).attaquer(guerriersFaction1.get(0));
                }

                //"Bring out yer dead !"
                //On vérifie si le premier guerrier de chaque faction est mort à la suite de cet échange, pour pouvoir le retirer de la phalange
                if(guerriersFaction1.get(0).isDead()) {
                    Menestrel.annoncerDeces(guerriersFaction1.get(0));
                    guerriersFaction1.remove(0);
                }

                if(guerriersFaction2.get(0).isDead()) {
                    Menestrel.annoncerDeces(guerriersFaction2.get(0));
                    guerriersFaction2.remove(0);
                }
            } catch (CoupDivin cp) {
                //System.out.println(cp.getMessage());
                Menestrel.narrerCoupDivin(cp);
                //C'est du bricolage, ça ! Il faut que je trouve comment récupérer la faction d'un guerrier...
                if (cp.getGuerrier().getCouleur() == "rouge") {
                    guerriersFaction2.clear();
                } else {
                    guerriersFaction1.clear();
                }
            }
            if(guerriersFaction1.isEmpty()) {
                Menestrel.rapporterHecatombe("rouge");
            }
            if(guerriersFaction2.isEmpty()) {
                Menestrel.rapporterHecatombe("bleu");
            }

            i++;
        }
    }
}
