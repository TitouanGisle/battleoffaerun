package Game.Classes;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

/**
 * Class Chateau
 */
public class Chateau {

    //
    // Fields
    //
    static private final int COUT_BASE = 1;
    static private final int RESSOURCES_BASES = 3;
    private int ressources;
    private String couleur;
    private LinkedList<Guerrier> salleDEntrainement;

    //
    // Constructors
    //
    public Chateau (String couleur) {
        this.couleur = couleur;
        ressources = RESSOURCES_BASES;
        salleDEntrainement = new LinkedList<Guerrier>();
    };

    //
    // Accessor methods
    //
    public String getCouleur() {
        return couleur;
    }

    /**
     * Set the value of ressources
     * @param newVar the new value of ressources
     */
    public void setRessources (int newVar) {
        ressources = newVar;
    }

    /**
     * Get the value of ressources
     * @return the value of ressources
     */
    public int getRessources () {
        return ressources;
    }

    public LinkedList<Guerrier> getSalleDEntrainement() {
        return salleDEntrainement;
    }
    //
    // Methods
    //

    /**
     * créer un nouveau guerrier du type déterminé
     * @param type
     * @param chef
     * @return
     */
    public Guerrier entrainer(int type, boolean chef) {
        Guerrier guerrier = (type==1) ? new GuerrierNain() : new GuerrierElf();
        guerrier.setChateau(this); guerrier.setChef(chef);
        salleDEntrainement.add(guerrier);

        return guerrier;
    }

    /**
     * Retourne une queue de guerrier(s) qu'il a été possible de diplomer compte tenu des ressources du château.
     * @return une ArrayList de Guerriers prêts au combat !
     */
    public ArrayList<Guerrier> diplomer() {
        ArrayList<Guerrier> guerriers = new ArrayList<>();
        Guerrier premierDeClasse = salleDEntrainement.peek();

        while(ressources>0 && premierDeClasse != null && premierDeClasse.getCout()<=ressources) {
            guerriers.add(salleDEntrainement.poll());
            setRessources(ressources-premierDeClasse.getCout());
            premierDeClasse = salleDEntrainement.peek();
        }

        return guerriers;
    }

    /**
     * ajoute les ressources par tour aux ressources du chateau
     * @return les ressources du chateau après impot
     */
    public int leverImpot() {
        return ++ressources;
    }


    /**
     * Entraine n troupes aléatoires dans le chateau
     * @return void
     */
    public void entrainementRandom(int n) {
        for(int i = 0; i<n ; i++) {
            Guerrier apprenti = entrainer(new Random().nextInt(2), new Random().nextBoolean());
        }
    }
}