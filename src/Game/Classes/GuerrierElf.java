package Game.Classes;

/**
 * Class GuerElf
 */
public class GuerrierElf extends Guerrier {
  //
  // Fields
  //

  static private int multAttaque = 2;
  
    //
    // Constructors
    //
    public GuerrierElf() {
        super();
    };

    public int getCout() {
        return isChef() ? BASE_COUT*4 : BASE_COUT*2;
    }

  /**
   * Get the value of multAttaque
   * Depending on whether the warrior is a chief or not
   * @return the value of multAttaque
   */
  protected int getMultAttaque ()
  {
    return isChef() ? multAttaque*2 : multAttaque;
  }

  /**
   * Génère une force propre aux elfes,
   * en fonction de l'attribut multAttaque de la Classe GuerElf,
   * et de la nature de chef, ou non, du guerrier
   * @return       int
   */
    public int getForce()
  {
      return BASE_FORCE *getMultAttaque();
  }

    /**
     * Génère une chaine utilisable dans une phrase
     * @return string
     */
    public String getGroupeNominal() {
        String gn = isChef() ? "un chef" : "un guerrier";
        gn = gn+" elfe "+getCouleur();
        return gn;
    }

    /**
     * Retourne un code pour reconnaître l'unité
     * N : chef nain    n : guerrier nain
     * E : chef elfe    e : guerrier elfe
     */
    public String getCode() {
        return (isChef()) ? "E"+this.getCouleur().toUpperCase().charAt(0) : "e"+this.getCouleur().toUpperCase().charAt(0);
    }

    //Attribue une valeur de temerite à l'unité (plus elle est elevée, plus l'unité s'avancera au front du combat)
    //Tel que N(x/100)>n(100/100)>n(50/100)>e(x/100)>E(x/100)
    public int getTemerite() {
        return isChef() ? 1000+getCurrentPV() : 2000+getCurrentPV();
    }
}

